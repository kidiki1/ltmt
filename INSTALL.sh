#!/bin/bash
set -e
set +x

echo "Installing python requirements"
if [ ! -d venv ]; then
  python3 -m venv venv
fi
source venv/bin/activate
pip install -r requirements.txt

# Follow instructions for your OS here: https://www.elastic.co/guide/en/elasticsearch/reference/current/targz.html
if [ ! -d es ]; then
  echo "Downloading and installing Elastic search"
  wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.8.0-linux-x86_64.tar.gz
  tar -xzf elasticsearch-7.8.0-linux-x86_64.tar.gz
  mv elasticsearch-7.8.0 es
  rm elasticsearch-7.8.0-linux-x86_64.tar.gz
fi

if [ ! -d data ]; then
  echo "Initializing db"
  mkdir -p data
  python init_db.py
fi

echo
echo "Installation successful! Now:"
echo "1. Start ./run_elasticsearch.sh in another terminal and then"
echo "2. [IF NOT IMPORTED] Run ./populate_db.py <relative path to csv file to import>"
echo "3. Run ./run_webapp.py"
