import sqlalchemy as db
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

from sqlalchemy import join
from sqlalchemy.sql import select


class equation(Base):
    __tablename__="equation"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)
    latexcontent = db.Column(db.String)
    tags = db.Column(db.String)
    origin = db.Column(db.String)
    description = db.Column(db.Text)
    def __init__(self,info_dict):
        self.name = info_dict['name']
        self.latexcontent = info_dict['latexcontent']
        self.tags = info_dict['tags']
        self.origin = info_dict['origin']
        self.description = info_dict['description']

