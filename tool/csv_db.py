	
import csv
import os
import json 

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from models import equation, Base

folder = '/home/louis/Documents/gitlab_thinktype/ltmt/tool'
os.chdir(folder)

csv_file = 'Equplus-List.csv'

str_sqlite = "sqlite:///db.sqlite"

engine = create_engine(str_sqlite)
Session = sessionmaker(bind=engine)
session = Session()


def add_equation(info_dict):
    obj_eq = equation(info_dict)
    session.add(obj_eq)
    session.commit()
    return True


def create_db(engine):
    Base.metadata.create_all(engine)

def reset_db(session, engine):
    session.close()
    os.remove("db.sqlite")
    engine = create_engine("sqlite:///db.sqlite")
    Session = sessionmaker(bind=engine)
    session = Session()
    Base.metadata.create_all(engine)
    return engine, session

def csv_dict(csv_file):
    reader = csv.reader(open(csv_file), delimiter=',',quotechar='"')
    result = []
    cpt = 0
    for row in reader:
        if cpt == 0:
            keys = row
            cpt=cpt+1
        else:
            temp_dict={}
            for i,key in enumerate(row):
                if keys[i] in ['Category','Sub-Category']:
                    temp_dict['tags'] = key
                else:
                    temp_dict[keys[i]] = key
            result.append(temp_dict)
    return result

def json_dict(json_file):
    read_file = open(json_file)
    reader = json.load(read_file)
    result = []
    keys=['name','latexcontent','origin','tags']
    keys_json=['name','latex','source','tags']
    for row in reader:
        temp_dict={}
        print(row)
        for i,key in enumerate(keys_json):
            print(key)
            if key == 'tags':
                temp_dict[keys[i]] =', '.join(row[key])
            else:
                temp_dict[keys[i]] = row[key]
        temp_dict['description']=""
        result.append(temp_dict)
    return result


def csv_to_db(csv_file,session):
    dict_result = csv_dict(csv_file)
    for item in dict_result:
        add_equation(item)

def json_to_db(json_file,session):
    dict_result = json_dict(json_file)
    for item in dict_result:
        add_equation(item)

# Run reset_db to create one database, and then run csv_to_db to populate it