from .models import Equation
from . import db


def get_data():
    columns = [m.key for m in Equation.__table__.columns]
    res_raw = Equation.query.all()
    res = []
    for line in res_raw:
        res.append([line.id, line.name, line.latexcontent, line.tags, line.origin, line.description])
    return columns, res


def add_equation(info_dict):
    obj_eq = Equation(**info_dict)
    db.session.add(obj_eq)
    db.session.commit()
    return True


def get_equation(eq_number):
    obj_eq = Equation.query.filter_by(id=eq_number).first()
    l_result = [obj_eq.id, obj_eq.name, obj_eq.latexcontent, obj_eq.tags, obj_eq.origin, obj_eq.description]
    return l_result
