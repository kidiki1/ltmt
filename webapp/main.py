import flask_babel
from flask import current_app, Blueprint, render_template, request, url_for, redirect, flash, jsonify

from . import db
from .models import Equation
from .queries import get_data, add_equation, get_equation

import os
import random

from flask import g
from .forms import SearchForm
from elasticsearch import Elasticsearch

# Adding a reference for use in flask blueprint
main = Blueprint('main', __name__)


@main.before_request
def before_app_request():
    g.search_form = SearchForm()
    g.locale = str(flask_babel.get_locale())


# Main route for welcome
@main.route('/')
def index():
    return render_template('index.html')


def error_404(e):
    '''Fonction pour l'erreur 404
    Utilisé dans le __init__.py en ref 404
    Fonctionne par chance
    '''
    return render_template('404.html'), 404


@main.route('/add_equation')
def render_add():
    return render_template('add_eq.html')


@main.route('/post_eq', methods=['POST'])
def add_eq():
    info_dict = {}
    info_dict['name'] = request.form.get('name')
    info_dict['latexcontent'] = request.form.get('content')
    info_dict['tags'] = request.form.get('tags')
    info_dict['origin'] = request.form.get('origin')
    add_equation(info_dict)
    Equation.reindex()
    return render_template('success_post.html')


@main.route('/equation/<int:eq_number>')
def equation_page(eq_number):
    result = get_equation(eq_number)
    print(result)
    return render_template('show_eq.html', eq_data=result)


@main.route('/reindex')
def reindex_f():
    Equation.reindex()
    return render_template('index.html')


# Search view function

@main.route('/search')
def search():
    if not g.search_form.validate():
        return redirect(url_for('main.table'))
    page = request.args.get('page', 1, type=int)
    print(page)
    q_data = request.form.get('q')
    equations, total = Equation.search(g.search_form.q.data, page, current_app.config['POSTS_PER_PAGE'])
    l_res = equations.all()
    l_final = []
    for item in l_res:
        t_dict = item.__dict__
        t_dict.pop('_sa_instance_state')
        l_final.append(t_dict)
    columns = ['id', 'name', 'latexcontent', 'tags', 'origin', 'description']
    next_url = url_for('main.search', q=g.search_form.q.data, page=page + 1) \
        if total > page * current_app.config['POSTS_PER_PAGE'] else None
    prev_url = url_for('main.search', q=g.search_form.q.data, page=page - 1) \
        if page > 1 else None
    return render_template('search.html', title=('Search'), equations=l_final, columns=columns,total=total,
                           next_url=next_url, prev_url=prev_url)


@main.route('/table')
def table_render():
    return render_template('table.html')


@main.route('/apitable', methods=['POST'])
def table_page():
    n_item = Equation.query.count()
    length_table =  int(request.form.get('length'))
    start_index = int(request.form.get('start'))
    print(start_index)
    res = Equation.query.filter(Equation.id > start_index).limit(length_table).all()
    columns = [m.key for m in Equation.__table__.columns]
    list_dict = []
    for i, item in enumerate(res):
        d = item.__dict__
        d.pop('_sa_instance_state')
        d['name']="<a href='/equation/"+ str(d['id']) +"'>"+d['name']+"</a>"
        print(d['name'])
        d['latexcontent'] = '<p>\( ' + d["latexcontent"] + ' \)</p> <p class="rawLatexCode"> ' + d[
            "latexcontent"] + ' </p>'
        list_dict.append(d)
    print(list_dict)
    # dict_res = res.__dict__
    return jsonify({'data': list_dict, 'recordsTotal': n_item})
