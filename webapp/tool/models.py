import sqlalchemy as db
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

from sqlalchemy import join
from sqlalchemy.sql import select

class User(UserMixin,db.Model):
    id = db.Column(db.Integer, primary_key=True) # primary keys are required by SQLAlchemy
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    name = db.Column(db.String(1000))
    def __init__(self, name, email, password):
        self.name = name
        self.email = email
        self.password = password

class equation(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)
    latexcontent = db.Column(db.String)
    upuser = db.Column(db.Integer, db.ForeignKey('user.id'))
    tags = db.Column(db.String)
    origin = db.Column(db.String)
