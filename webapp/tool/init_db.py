
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from pathlib import Path    
import os 
from os import listdir
from os.path import isfile, join

from models import Base, User, dataclass

str_sqlite = "sqlite:///db.sqlite"

engine = create_engine(str_sqlite)
Session = sessionmaker(bind=engine)
session = Session()


def reset_db(session, engine):
    session.close()
    os.remove("db.sqlite")
    engine = create_engine("sqlite:///db.sqlite")
    Session = sessionmaker(bind=engine)
    session = Session()
    Base.metadata.create_all(engine)
    return engine, session