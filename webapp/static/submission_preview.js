(function(){
	let latexInput = document.getElementById("content");
	let preview = document.getElementById("content-preview");
	let lastText = "";
	let updateStyle = function(newVal){
		console.log(lastText);
		if(newVal == lastText){
			preview.className = "preview-upToDate";
		}else{
			preview.className = "preview-outdated";
		}
	};
	latexInput.oninput = function(){
		let content = latexInput.value;
		updateStyle(content);
	};
	latexInput.onchange = function(){
		let content = latexInput.value;
		preview.innerHTML = "";
		preview.innerText = "\\[ " + content + " \\]";
		lastText = content;
		updateStyle(content);
		MathJax.typeset();
	};
})();
