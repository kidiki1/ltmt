function injectCopyButton(){
	let numberOfButtons = 0;
	let latexCode = document.getElementsByClassName("rawLatexCode");
	for(let lc of latexCode) {
		let content = lc.innerHTML;
		lc.innerHTML = content + " <button type=\"button\" class=\"clipboard-button\" id=\"clipboard-button-" + numberOfButtons + "\"> Copy to clipboard</button>"
		let button = document.getElementById("clipboard-button-" + numberOfButtons);
		numberOfButtons++;
		button.onclick = function(){
			navigator.clipboard.writeText(content);
		};
	}
}
