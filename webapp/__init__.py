# init SQLAlchemy so we can use it later in our models
# importing elasticsearch configuration
import flask_babel

from config import Config
from elasticsearch import Elasticsearch
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

## Defining the database object for further uses
db = SQLAlchemy()


def create_min_app(config_class=Config):
    app = Flask(__name__)

    ## Configuration of Flask Application
    app.config.from_object(config_class)

    ## Initialisation of Database
    db.init_app(app)

    return app


def create_app(config_class=Config):
    app = create_min_app(config_class)

    # blueprint for non-auth parts of app
    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    # blueprint for 404 part of app
    from .main import error_404
    app.register_error_handler(404, error_404)

    app.elasticsearch = Elasticsearch([app.config['ELASTICSEARCH_URL']])

    flask_babel.Babel(app)

    return app


app = create_app()
