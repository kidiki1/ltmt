import os
from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))

DATA_DIR = os.path.join(basedir, 'data')
STATIC_DATA_DIR = os.path.join(basedir, 'static_data')


class Config(object):
    SQLALCHEMY_DATABASE_URI = "sqlite:///{}".format(os.path.join(DATA_DIR, "db.sqlite"))
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    ELASTICSEARCH_URL = 'http://localhost:9200'

    LANGUAGES = ['en', 'es']
    POSTS_PER_PAGE = 25
