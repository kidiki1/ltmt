#!/bin/bash

if [ ! -d venv ]; then
  echo "Run ./INSTALL.sh first to create a venv"
  exit 1;
fi
source venv/bin/activate
export FLASK_DEBUG=1
flask run