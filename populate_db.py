#!venv/bin/python
import csv
import os
import sys

from config import basedir
from webapp import db, app
from webapp.models import Equation

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Use: ./populate_db.py <filename inside static_data>")
        exit(1)
    filename = sys.argv[1]
    with app.app_context():
        with open(os.path.join(basedir, filename)) as csvfile:
            csvreader = csv.DictReader(csvfile)
            for row in csvreader:
                equation = Equation(name=row.get('name'),
                                    description=row.get('description'),
                                    latexcontent=row.get('latexcontent'),
                                    )
                db.session.add(equation)
        db.session.commit()
