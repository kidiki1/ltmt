from webapp import db, create_min_app

if __name__ == "__main__":
    app = create_min_app()
    with app.app_context():
        db.create_all()
