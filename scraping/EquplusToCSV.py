import json
import numpy
import csv

equplusList = json.load(open("Equplus-List.json"))
someGasKinetics = json.load(open("SomeGasKinetics.json"))

count = 0

def toTableFormat(j):
	global count
	count = count + 1
	tags = ""
	for i in j["tags"]:
		if tags != "":
			tags = tags + ", "
		tags = tags + i
	description = j["description"] if "description" in j else ('No Description yet! Source: ' + j['source'])
	return {
		'id' : count,
		'name' : j['name'],
		'description' : description,
		'latexcontent' : j['latex'],
		'upuser' : 1,
		'tags' : tags,
		'origin' : 'Unknown'
	}
	


with open('../static_data/Equplus-List.csv', 'w') as csvfile:
	fieldnames = ['id', 'name', 'latexcontent', 'description', 'upuser', 'tags', 'origin']
	writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
	writer.writeheader()
	for j in equplusList:
		if (not ("ignoreForNow" in j)) or not j["ignoreForNow"]:
			writer.writerow(toTableFormat(j))
	for j in someGasKinetics:
		writer.writerow(toTableFormat(j))
