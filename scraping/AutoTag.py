import json

equplusList = json.load(open("Equplus-List.json"))

def addTag(e, t):
	if not (t in e["tags"]):
		e["tags"].append(t)

for e in equplusList:
	if not ("tags" in e):
		e["tags"] = []
	# Everything has a tags key now
	latex = e["latex"]
	# Trigonometric
	if latex.find("\\sin") != -1:
		addTag(e, "Trigonometric")
	elif latex.find("\\cos") != -1:
		addTag(e, "Trigonometric")
	elif latex.find("\\tan") != -1:
		addTag(e, "Trigonometric")
	elif latex.find("\\cot") != -1:
		addTag(e, "Trigonometric")
	elif latex.find("\\sec") != -1:
		addTag(e, "Trigonometric")
	elif latex.find("\\csc") != -1:
		addTag(e, "Trigonometric")
	# Partial differential
	if latex.find("\\partial") != -1:
		addTag(e, "Partial Differential")
		addTag(e, "Differential")
	# Integral
	if latex.find("\\int") != -1:
		addTag(e, "Integral")
	elif latex.find("\\oint") != -1:
		addTag(e, "Integral")
	# Integral
	if latex.find("{\\lim }") != -1:
		addTag(e, "Limit")

outFile = open("Equplus-List-Autotagged.json", "w")
outFile.write(json.dumps(equplusList, indent=2))
outFile.close()

