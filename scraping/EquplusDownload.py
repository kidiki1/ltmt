import os
import json

def extract(num):
	print(num)
	url = "https://equplus.net/eqninfo/Equation-"+str(num)+".html"
	text = os.popen("curl " + url).read().split("\n")
	if len(text) < 51:
		return None
	nameline = text[49]
	latexline = text[51]
	name = nameline[nameline.find("<h1>")+4 : nameline.find("</h1>")]
	latexStart = latexline.find("<p class=\"latex\">")+17
	# deal with code going over multiple lines
	if latexline.find("</p>") != -1:
		latex = latexline[latexStart:latexline.rfind("</p>")]
	else:
		latex = latexline[latexStart:]
		line = 52
		notDone = True
		while(notDone):
			latexEnd = text[line].find("</p>")
			if latexEnd == -1:
				latex = latex + " " + text[line]
				line = line + 1
			else:
				latex = latex + (" " if latexEnd > 0 else "") +  text[line][:latexEnd]
				notDone = False
	return {"name" : name, "latex" : latex, "source" : url}

list1 = [extract(i) for i in range(1, 500)]
equplusList = [i for i in list1 if i is not None]

outFile = open("Equplus-List.json", "w")
outFile.write(json.dumps(equplusList, indent=2))
outFile.close()
